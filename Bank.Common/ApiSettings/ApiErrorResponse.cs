﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bank.Common.ApiSettings
{
    /// <summary>
    /// Standard error response container
    /// </summary>
    [DataContract]
    public class ApiErrorResponse
    {
        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        [DataMember(Name = "error")]
        public ErrorDescription Error
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiErrorResponse"/> class.
        /// </summary>
        public ApiErrorResponse()
        {
            Error = new ErrorDescription();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ErrorDescription
    {
        public int StatusCode { get; set; }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        [DataMember(Name = "code", Order = 10)]
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [DataMember(Name = "message", Order = 20)]
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [DataMember(Name = "type", Order = 30)]
        public string ErrorType { get; set; }
    }
}
