﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Model.Banking
{
    public interface IBank
    {   
        void Withdraw(double amount);
        void Deposit(double amount);
        double GetBalance();
    }
}
