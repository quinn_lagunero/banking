﻿using Bank.Common.Cqrs;
using Bank.Common.Cqrs.Validations;
using Bank.Domain.Model.Account;
using Bank.Domain.Model.Banking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Command.Deposit
{
    public class WithdrawCommandValidator : BaseCommandValidator, IValidate<WithdrawCommand>
    {
        public WithdrawCommandValidator()
        {

        }
        public IReadOnlyCollection<IValidationResult> Validate(WithdrawCommand command)
        {
            if (command.Amount == 0)
                throw new ArgumentNullException("Withdrawal Amount", "Amount to be Withdrawn cannot be 0");

            if (command.Amount < TransactionRules.MinimumWithdrawal)
                throw new ArgumentNullException("Withdrawal Amount", string.Format("Amount to be Withdrawn:{0} cannot be less than {1}"
                    , command.Amount, TransactionRules.MinimumWithdrawal));

            if (command.Amount > TransactionRules.MaximumWithdrawal)
                throw new ArgumentNullException("Withdrawal Amount", string.Format("Amount to be Withdrawn: {0} exceeds maximum ammount of {1}" +
                    "", command.Amount, TransactionRules.MaximumWithdrawal));


            if (command.Amount > Balance.CurrentBalance)
                throw new ArgumentNullException("Withdrawal Amount", string.Format("Amount to be Withdrawn: {0} exceeds current balance of {1}" +
                    "", command.Amount, Balance.CurrentBalance));

            return new List<IValidationResult>();
        }
    }
}
