﻿using Bank.Common.Cqrs.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Common.Cqrs
{
    public interface IValidate<T>
    {
        /// <summary>
        /// Validates the specified instance.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        IReadOnlyCollection<IValidationResult> Validate(T command);
    }
}
