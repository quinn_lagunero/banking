﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Bank.Common.ApiSettings
{
    /// <summary>
    /// Standard class for all api responses
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class ApiResponse<T>
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        [DataMember(Name = "data")]
        public T Data { get; set; }
    }
}
