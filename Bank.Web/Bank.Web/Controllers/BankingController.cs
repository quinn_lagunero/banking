﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Bank.Web.Models;
using Bank.Web.Models.Banking;
using Bank.Domain.Dal.Banking.Command;
using Bank.Common.ApiSettings;
using Bank.Domain.Model.Account;
using System.Net;

namespace Bank.Web.Controllers
{
    public class BankingController : Controller
    {
        #region Variables

        private const string ViewPrefix = "~/Views/Banking/";
        private const string ViewSuffix = ".cshtml";
        private IConfiguration _configuration;
        protected ClientApi _clientApi = null;

        #endregion Variables

        #region Constructor

        public BankingController(IConfiguration configuration)
        {
            _configuration = configuration;

            _clientApi = new ClientApi(configuration.GetConnectionString("Banking_API"), Guid.NewGuid().ToString()); //for now auto generate always a token but it shoudn't be this way
        }

        #endregion Constructor

        #region Get

        public async Task<IActionResult> Index()
        {
            ViewBanking model = new ViewBanking();

            string response = await _clientApi.CallApiGetString<string>("banking");

            model.CurrentBalance = response.Replace(@"""", string.Empty);

            return View(ViewPrefix + "Index" + ViewSuffix, model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion Get

        #region Post

        [HttpPost("deposit")]
        public async Task<IActionResult> Deposit(ViewBanking viewCommand)
        {
            DepositCommand command = new DepositCommand();
            command.Amount = viewCommand.AmountToDeposit.Value;

            string json = JsonConvert.SerializeObject(command);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

            string response
                = await _clientApi.CallApiPostString<string>("banking/deposit", content);

            if (string.IsNullOrWhiteSpace(response) || (response.Contains("StatusCode")
                && int.Parse(JObject.Parse(response).SelectToken("StatusCode").ToString()) != (int)HttpStatusCode.OK))
                return BadRequest(JObject.Parse(response).SelectToken("Message").ToString());

            return Json(response.Replace(@"""", string.Empty));
        }

        [HttpPost("withdraw")]
        public async Task<IActionResult> Withdraw(ViewBanking viewCommand)
        {
            WithdrawCommand command = new WithdrawCommand();
            command.Amount = viewCommand.AmountToWithdraw.Value;

            string json = JsonConvert.SerializeObject(command);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

            string response
                 = await _clientApi.CallApiPutString<string>("banking/withdraw", content);

            if (string.IsNullOrWhiteSpace(response) || (response.Contains("StatusCode")
                && int.Parse(JObject.Parse(response).SelectToken("StatusCode").ToString()) != (int)HttpStatusCode.OK))
                return BadRequest(JObject.Parse(response).SelectToken("Message").ToString());

            return Json(response.Replace(@"""", string.Empty));
        }


        #endregion Post
    }
}
