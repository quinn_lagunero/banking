﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace Bank.Common
{
    public class SystemSettings
    {
        public static string CurrentUserName { get { return WindowsIdentity.GetCurrent().Name; } }
    }
}
