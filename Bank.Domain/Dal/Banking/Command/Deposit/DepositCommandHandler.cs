﻿using Bank.Common.Cqrs;
using Bank.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Command
{
    public class DepositCommandHandler : ICommandHandler<DepositCommand>
    {
        public void Execute(DepositCommand command)
        {
            Bank.Domain.Model.Banking.Bank model = new Bank.Domain.Model.Banking.Bank();

            model.Deposit(command.Amount);
        }
    }
}
