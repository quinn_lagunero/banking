﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Model.Banking
{
    public static class TransactionRules
    {
        public const double MinimumDeposit = 1000;
        public const double MaximumDeposit = 50000;
        public const double MinimumWithdrawal = 300;
        public const double MaximumWithdrawal = 10000;
    }
}
