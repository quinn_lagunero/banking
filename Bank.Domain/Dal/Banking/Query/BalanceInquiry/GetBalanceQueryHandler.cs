﻿using Bank.Common.Cqrs;
using Bank.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Bank.Domain.Dal.Banking.Query
{
    public class GetBalanceQueryHandler : IQueryHandler<GetBalanceQuery, double>
    {
        #region Constructor

        public GetBalanceQueryHandler()
        {

        }

        #endregion Constructor

        #region Constructor

        public double Handle(GetBalanceQuery query)
        {
            return Balance.CurrentBalance;
        }

        #endregion Constructor
    }
}
