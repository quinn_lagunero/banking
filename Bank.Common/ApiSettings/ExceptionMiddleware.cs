﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static Bank.Common.CustomExtensions;

namespace Bank.Common.ApiSettings
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private IConfiguration _configuration;

        public ExceptionMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                LogError(httpContext, ex);
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = exception.Message
            }.ToString());
        }

        private void LogError(HttpContext httpContext, Exception ex)
        {
            string uri = GetCurrentHost(httpContext).ToString();
            var outputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u4}] | {Message:l}{NewLine}{Exception}";
            string loggingPath = _configuration.GetSection("Logging").GetSection("LoggingPath").Value;

            Log.Logger = new LoggerConfiguration()
                    .WriteTo.File(loggingPath,
                              outputTemplate: outputTemplate,
                              rollingInterval: RollingInterval.Day)
                   .WriteTo.Seq(uri)
                   .CreateLogger();

            Log.Logger.Information("Executed at {ExecutionTime}", Environment.TickCount);
            Log.Logger.Error("Error: {0} at {1}", ex.Message, GetAbsoluteUri(httpContext).ToString());
        }

        private Uri GetCurrentHost(HttpContext httpContext)
        {
            var request = httpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.Host;
            uriBuilder.Port = request.Host.Port.Value;
            return uriBuilder.Uri;
        }

        private static Uri GetAbsoluteUri(HttpContext httpContext)
        {
            var request = httpContext.Request;
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.Host;
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri;
        }

    }
}
