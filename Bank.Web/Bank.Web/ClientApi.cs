﻿using Bank.Common.ApiSettings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace Bank.Web
{
    public class ClientApi
    {
        private const string userAgent = "Retail-Store";

        #region Variables

        string _url = string.Empty;
        string _token = string.Empty;

        #endregion Variables

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientApi" /> class.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="token">The token.</param>
        public ClientApi(string url, string token)
        {
            _url = url;
            _token = token;
        }

        #endregion Constructors

        #region Public
        /// <summary>
        /// Calls the API get string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resourceUri">The resource URI.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        public async Task<string> CallApiGetString<T>(string resourceUri)
        {
            string data = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("USER-AGENT", userAgent);
                if (!string.IsNullOrEmpty(_token))
                    client.DefaultRequestHeaders.Add("APITOKEN", _token);

                try
                {
                    HttpResponseMessage response = await client.GetAsync(resourceUri);

                    if (response.IsSuccessStatusCode)
                        data = await response.Content.ReadAsStringAsync();

                }
                catch (HttpRequestException requestException)
                {
                    string exMessage = requestException.Message;
                    if (requestException.InnerException != null)
                        exMessage = string.Format("{0}-{1}", exMessage, requestException.InnerException.Message);

                    return exMessage;
                }
            }
            return data;
        }


        public async Task<string> CallApiPostString<T>(string resourceUri, HttpContent content)
        {
            string data = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("USER-AGENT", userAgent);
                if (!string.IsNullOrEmpty(_token))
                    client.DefaultRequestHeaders.Add("APITOKEN", _token);

                try
                {
                    HttpResponseMessage response = await client.PostAsync(resourceUri, content);

                    data = await response.Content.ReadAsStringAsync();

                }
                catch (HttpRequestException requestException)
                {
                    string exMessage = requestException.Message;
                    if (requestException.InnerException != null)
                        exMessage = string.Format("{0}-{1}", exMessage, requestException.InnerException.Message);

                    return exMessage;
                }
            }
            return data;
        }

        public async Task<string> CallApiPutString<T>(string resourceUri, HttpContent content)
        {
            string data = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("USER-AGENT", userAgent);
                if (!string.IsNullOrEmpty(_token))
                    client.DefaultRequestHeaders.Add("APITOKEN", _token);

                try
                {
                    HttpResponseMessage response = await client.PutAsync(resourceUri, content);

                    data = await response.Content.ReadAsStringAsync();

                }
                catch (HttpRequestException requestException)
                {
                    string exMessage = requestException.Message;
                    if (requestException.InnerException != null)
                        exMessage = string.Format("{0}-{1}", exMessage, requestException.InnerException.Message);

                    return exMessage;
                }
            }
            return data;
        }
        #endregion Public
    }
}
