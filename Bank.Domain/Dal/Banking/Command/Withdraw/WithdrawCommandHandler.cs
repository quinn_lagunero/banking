﻿using Bank.Common.Cqrs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Command
{
    public class WithdrawCommandHandler : ICommandHandler<WithdrawCommand>
    {
        public void Execute(WithdrawCommand command)
        {
            Bank.Domain.Model.Banking.Bank model = new Bank.Domain.Model.Banking.Bank();

            model.Withdraw(command.Amount);
        }
    }
}
