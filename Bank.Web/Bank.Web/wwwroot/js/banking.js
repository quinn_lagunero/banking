﻿

function IsNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$(':input[type="number"]').on('keydown keyup', function (e) {
    if (!IsNumberKey($(this).val())) {
        return false;
    }
});



$(document).on('click', '#btn-withdraw', function () {
    if ($('#txt-withdraw-amount').val() == "") {
        $.notify("Please input withdrawal amount", "error");

        return false;
    }

    var actionUrl = $('#pnl-banking').data('withdraw');
    var commandData = new Object();
    commandData.amountToWithdraw = parseFloat($('#txt-withdraw-amount').val());

    $("#divLoading").show();
    $.ajax({
        type: "POST",
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        url: actionUrl,
        data: commandData,
        success: function (response) {
            $("#divLoading").hide();
            $('#txt-withdraw-amount').val("");
            $('#div-current-balance').html(response);
        },
        error: function (xhr, status, response) {
            $("#divLoading").hide();
            $.notify(xhr.responseText, "error");
        }
    });
});

$(document).on('click', '#btn-deposit', function () {
    if ($('#txt-deposit-amount').val() == "") {
        $.notify("Please input deposit amount", "error");

        return false;
    }

    var actionUrl = $('#pnl-banking').data('deposit');
    var commandData = new Object();
    commandData.amountToDeposit = parseFloat($('#txt-deposit-amount').val());

    $("#divLoading").show();
    $.ajax({
        type: "POST",
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        url: actionUrl,
        data: commandData,
        success: function (response) {
            $("#divLoading").hide();
            $('#txt-deposit-amount').val("");
            $('#div-current-balance').html(response);
        },
        error: function (xhr, status, response) {
            $("#divLoading").hide();
            $.notify(xhr.responseText, "error");
        }
    });
});