﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Bank.Common.Cqrs;
using Bank.Domain.Dal.Banking.Query;
using Bank.Domain.Model.Account;

namespace Bank.Api.Controllers.Query
{
    [Route("banking")]
    [ApiController]
    public class TranscationQueryController : Controller
    {
        #region Variables

        private IQueryHandler<GetBalanceQuery, double> _handlerGetBalance = null;

        #endregion Variables

        #region Constructor

        public TranscationQueryController(IQueryHandler<GetBalanceQuery, double> handlerGetBalance)
        {
            _handlerGetBalance = handlerGetBalance;
        }

        #endregion Constructor

        #region Get

        /// <summary>
        /// Gets the current balance
        /// </summary>
        /// <param name="allowedDecimal"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(object), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetCurrentBalance(int allowedDecimal)
        {
            GetBalanceQuery query = new GetBalanceQuery()
            {
                AllowedDecimalPlaces = allowedDecimal
            };

            _handlerGetBalance.Handle(query);

            return Ok(Balance.CreateNumberFormat(allowedDecimal));
        }

        #endregion Get
    }
}
