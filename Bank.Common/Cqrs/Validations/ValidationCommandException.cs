﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bank.Common.Cqrs.Validations
{
    [Serializable]
    public class ValidationCommandException : Exception
    {
        #region Variables

        private IReadOnlyCollection<IValidationResult> _validationResults;

        #endregion Variables

        #region Constructors

        public ValidationCommandException(IReadOnlyCollection<IValidationResult> validationResults)
        {
            _validationResults = validationResults;
        }

        public ValidationCommandException(string message)
            : base(message)
        {
        }
        public ValidationCommandException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected ValidationCommandException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion Constructors
    }
}