﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Common.Cqrs
{
    /// <summary>
    /// Command interface for executing Create/Update/Delete commands
    /// </summary>
    /// <typeparam name="TCommand">The type of the command.</typeparam>
    public interface ICommandHandler<TCommand>
    {
        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        void Execute(TCommand command);
    }

}
