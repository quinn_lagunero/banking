﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bank.Common.ApiSettings
{
    public class ClientApiResponse<T>
    {
        private HttpRequestException _requestException = null;
        private ApiErrorResponse _error;
        private T _result = default(T);
        private bool _isSuccess = false;

        /// <summary>
        /// Gets the request exception.
        /// </summary> 
        /// <value>
        /// The request exception.
        /// </value>
        public HttpRequestException RequestException
        {
            get
            {
                return this._requestException;
            }
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public ApiErrorResponse Error
        {
            get
            {
                return this._error;
            }
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public T Result
        {
            get
            {
                return this._result;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess
        {
            get
            {
                return this._isSuccess;
            }
        }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientApiResponse{T}"/> class.
        /// </summary>
        /// <remarks>Success response</remarks>
        /// <param name="result">The result.</param>
        public ClientApiResponse(T result)
        {
            this._result = result;
            this._isSuccess = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientApiResponse{T}" /> class.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <remarks>
        /// Success response
        /// </remarks>
        public ClientApiResponse(ApiResponse<T> result)
        {
            if (result != null && result.Data != null)
                this._result = result.Data;

            this._isSuccess = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientApiResponse{T}"/> class.
        /// </summary>
        /// <remarks>Fail response</remarks>
        /// <param name="exception">The exception.</param>
        public ClientApiResponse(HttpRequestException exception)
        {
            this._requestException = exception;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientApiResponse{T}"/> class.
        /// </summary>
        /// <param name="error">The error.</param>
        public ClientApiResponse(ApiErrorResponse error)
        {
            this._error = error;
        }

        #endregion Constructors
    }
}
