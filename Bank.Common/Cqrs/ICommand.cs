﻿using System;
using System.Collections.Generic;
using System.Text;
using Bank.Common.Cqrs.Validations;

namespace Bank.Common.Cqrs
{
    public interface ICommand
    {
        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid { get; }

        /// <summary>
        /// Event fired prior to executing the command
        /// </summary>
        /// <returns></returns>
        IValidationResult BeforeExecute();

        /// <summary>
        /// Event fired prior after executing the command
        /// </summary>
        /// <returns></returns>
        IValidationResult AfterExecute();
    }
}
