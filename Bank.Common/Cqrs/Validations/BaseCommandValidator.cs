﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bank.Common.Cqrs.Validations
{
    public class BaseCommandValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseCommandValidator"/> class.
        /// </summary>
        public BaseCommandValidator()
        {
        }

        /// <summary>
        /// Validates the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        protected virtual IReadOnlyCollection<IValidationResult> Validate(ICommand command)
        {
            this.ValidateDataAnnotations(command);

            return new List<IValidationResult>() { new SuccessResult() };
        }

        /// <summary>
        /// Validates the data annotations.
        /// </summary>
        /// <param name="instance">The instance.</param>
        protected void ValidateDataAnnotations(object instance)
        {
            var context = new ValidationContext(instance, null, null);

            // Throws an exception when instance is invalid.
            Validator.ValidateObject(instance, context);
        }
    }
}
