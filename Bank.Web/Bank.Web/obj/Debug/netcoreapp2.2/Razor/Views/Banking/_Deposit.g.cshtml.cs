#pragma checksum "E:\Projects\banking\Bank.Web\Bank.Web\Views\Banking\_Deposit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4ceeb1c8e03de27c4c1655ea2afa8343e24811dc"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Banking__Deposit), @"mvc.1.0.view", @"/Views/Banking/_Deposit.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Banking/_Deposit.cshtml", typeof(AspNetCore.Views_Banking__Deposit))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\Projects\banking\Bank.Web\Bank.Web\Views\_ViewImports.cshtml"
using Bank.Web;

#line default
#line hidden
#line 2 "E:\Projects\banking\Bank.Web\Bank.Web\Views\_ViewImports.cshtml"
using Bank.Web.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4ceeb1c8e03de27c4c1655ea2afa8343e24811dc", @"/Views/Banking/_Deposit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed06003288b2725938159a62ddbfa06397d54c31", @"/Views/_ViewImports.cshtml")]
    public class Views_Banking__Deposit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 1049, true);
            WriteLiteral(@"<div class=""col-xs-9 col-md-8"">
    <div class=""card-body"">
        <div class=""card"">
            <div class=""row"">
                <div class=""card-body property-card"">
                    <div class=""form-group"">
                        <div class=""col-sm-12"">
                            <div class=""row row-no-gutters"">
                                <div class=""col-sm-6"">
                                    <label class=""control-label col-sm-12"">Input your deposit amount: </label>
                                </div>
                                <div class=""col-sm-6"">
                                    <input type=""number"" class=""form-control""  id=""txt-deposit-amount"" nkeypress=""return IsNumberKey(event)"" min=""300"" max=""10000"" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type=""button"" id=""btn-deposit"">Deposit</button>
        <");
            WriteLiteral("/div>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
