﻿using Bank.Common.Cqrs.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank.Common.Cqrs.Decorators
{
    public class ValidationCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> decorated;
        private readonly IValidate<TCommand> validator;

        public ValidationCommandHandlerDecorator(ICommandHandler<TCommand> decorated, IValidate<TCommand> validator)
        {
            this.decorated = decorated;
            this.validator = validator;
        }

        public void Execute(TCommand command)
        {
            var results = validator.Validate(command);

            if (HasAnyErrors(results))
                throw new ValidationCommandException(results);

            decorated.Execute(command);
        }

        private bool HasAnyErrors(IEnumerable<IValidationResult> results)
        {
            if (results == null || results.Count() == 0)
                return false;

            if (results.Any(r => !r.IsValid()))
                return true;

            return false;
        }
    }
}
