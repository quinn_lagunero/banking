﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Web.Models.Banking
{
    public class ViewBanking
    {
        public string CurrentBalance { get; set; }
        public double? AmountToDeposit { get; set; }
        public double? AmountToWithdraw { get; set; }
    }
}
