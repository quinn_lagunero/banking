﻿using Bank.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Model.Banking
{
    public class Bank : IBank
    {
        public void Deposit(double amount)
        {
            Balance.CurrentBalance += amount;
        }

        public double GetBalance()
        {
            return Balance.CurrentBalance;
        }

        public void Withdraw(double amount)
        {
            Balance.CurrentBalance -= amount;
        }
    }
}
