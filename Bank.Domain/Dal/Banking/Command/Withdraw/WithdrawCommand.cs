﻿using Bank.Common.Cqrs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Command
{
    public class WithdrawCommand : BaseCommand, ICommand
    {
        public double Amount { get; set; }
    }
}
