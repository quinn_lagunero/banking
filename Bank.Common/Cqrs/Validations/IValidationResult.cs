﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Bank.Common.Cqrs.Validations
{
    public interface IValidationResult
    {
        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns></returns>
        bool IsValid();

        /// <summary>
        /// Gets the result message.
        /// </summary>
        /// <value>
        /// The result message.
        /// </value>
        string ResultMessage { get; }
        /// <summary>
        /// Gets the API status code.
        /// </summary>
        /// <value>
        /// The API status code.
        /// </value>
        HttpStatusCode ApiStatusCode { get; }
    }
}
