﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;
using Swashbuckle.AspNetCore.Swagger;
using Bank.Common.Cqrs;
using Bank.Common;
using Microsoft.AspNetCore.StaticFiles;
using Bank.Common.ApiSettings;
using Bank.Common.Cqrs.Validations;
using Bank.Common.Cqrs.Decorators;

namespace Bank.Api
{
    public class Startup
    {
        private Container container = new Container();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            IntegrateSimpleInjector(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Banking API",
                    Description = "Deposits, Withdraw and get the current balance",
                    TermsOfService = "None",
                    Contact = new Contact
                    { 
                        Name = "Quinn Lagunero",
                        Email = "quinn_lagunero@yahoo.com",
                        Url = "https://www.youtube.com/channel/UC896587M2THOqyYpgCTd7Zw/channels?view_as=subscriber"
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            InitializeContainer(app);

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".scss"] = "text/css";
            provider.Mappings[".less"] = "text/css";
            app.UseStaticFiles(new StaticFileOptions()
            {
                ContentTypeProvider = provider
            });

            container.Verify();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseMvc();
        }

        #region Private

        private void InitializeContainer(IApplicationBuilder app)
        {
            System.Reflection.Assembly[] allAssemblies
                 = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.StartsWithInvariantText("Bank.")).ToArray();

            container.RegisterMvcControllers(app);
            container.RegisterMvcViewComponents(app);

            container.Register(typeof(IQueryHandler<,>), allAssemblies, Lifestyle.Scoped);
            container.Register(typeof(IQueryHandler<>), allAssemblies, Lifestyle.Scoped);
            container.Register(typeof(ICommandHandler<>), allAssemblies, Lifestyle.Scoped);
            container.Register(typeof(IValidate<>), allAssemblies, Lifestyle.Scoped);


            container.RegisterDecorator(typeof(ICommandHandler<>),
            typeof(ValidationCommandHandlerDecorator<>),
            Lifestyle.Scoped,
            context => context.ImplementationType != typeof(ValidationCommandHandlerDecorator<>));


            container.RegisterConditional(typeof(IValidate<>), typeof(NullValidator<>), Lifestyle.Scoped, c => !c.Handled);

            // Allow Simple Injector to resolve services from ASP.NET Core.
            container.AutoCrossWireAspNetComponents(app);
        }

        /// <summary>
        /// Integrates the simple injector.
        /// </summary>
        /// <param name="services">The services.</param>
        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpContextAccessor();

            services.AddSingleton<IControllerActivator>(
                new SimpleInjectorControllerActivator(container));
            services.AddSingleton<IViewComponentActivator>(
                new SimpleInjectorViewComponentActivator(container));

            services.EnableSimpleInjectorCrossWiring(container);
            services.UseSimpleInjectorAspNetRequestScoping(container);
        }

        #endregion Private
    }
}
