﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Bank.Domain.Model.Account
{
    public static class Balance 
    {
        public static double CurrentBalance = 5000.00;

        public static string CreateNumberFormat(int? allowedDecimal = null)
        {
            NumberFormatInfo precision = new NumberFormatInfo()
            {
                NumberDecimalDigits = allowedDecimal ?? 2
            };

            return Balance.CurrentBalance.ToString("N", precision);
        }
    }
}
