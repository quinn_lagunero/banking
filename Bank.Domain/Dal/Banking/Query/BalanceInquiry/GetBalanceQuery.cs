﻿using Bank.Common.Cqrs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Query
{
    public class GetBalanceQuery : BaseQuery, IQuery<double>
    {
        public int AllowedDecimalPlaces { get; set; }
    }
}
