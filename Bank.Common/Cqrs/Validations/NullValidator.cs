﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Bank.Common.Cqrs.Validations
{
    public class NullValidator<T> : IValidate<T>
    {
        /// <summary>
        /// Validates the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public IReadOnlyCollection<IValidationResult> Validate(T command)
        {
            // validate data annotations
            var context = new ValidationContext(command, null, null);

            // Throws an exception when instance is invalid.
            Validator.ValidateObject(command, context);

            return new List<IValidationResult>() { new SuccessResult() };
        }
    }
}
