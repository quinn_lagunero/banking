﻿using Bank.Common.Cqrs;
using Bank.Domain.Dal.Banking.Command;
using Bank.Domain.Model.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Bank.Api.Controllers.Command
{
    [Route("banking")]
    [ApiController]
    public class TranscationCommandController : Controller
    {
        #region Variables

        private ICommandHandler<DepositCommand> _handlerDeposit;
        private ICommandHandler<WithdrawCommand> _handlerWithdraw;

        #endregion Variables

        #region Constructor

        public TranscationCommandController(ICommandHandler<DepositCommand> handlerDeposit
            , ICommandHandler<WithdrawCommand> handlerWithdraw)
        {
            _handlerDeposit = handlerDeposit;
            _handlerWithdraw = handlerWithdraw;
        }

        #endregion Constructor

        #region Post

        /// <summary>
        /// Deposit amount
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("deposit")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Deposit(DepositCommand command)
        {
            _handlerDeposit.Execute(command);

            return Ok(Balance.CreateNumberFormat());
        }

        #endregion Post

        #region Put

        /// <summary>
        /// Withrawal amount
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("withdraw")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Withdraw(WithdrawCommand command)
        {
            _handlerWithdraw.Execute(command);

            return Ok(Balance.CreateNumberFormat());
        }

        #endregion Put
    }
}
