﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Common
{
    public static class CustomExtensions
    {
        public static bool StartsWithInvariantText(this string str, string value)
        {
            return str.StartsWith(value, true, System.Globalization.CultureInfo.InvariantCulture);
        }

        public class ErrorDetails
        {
            public int StatusCode { get; set; }
            public string Message { get; set; }


            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
            }
        }

    }
}
