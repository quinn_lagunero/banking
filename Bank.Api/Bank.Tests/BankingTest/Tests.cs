﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Bank.Api;
using Bank.Common;
using Xunit;
using Xunit.Abstractions;
using Xunit.Priority;
using Newtonsoft.Json;

namespace Bank.Tests.BankingTest
{
    [TestCaseOrderer(PriorityOrderer.Name, PriorityOrderer.Assembly)]
    public class Tests : IClassFixture<TestHelper<Startup>>
    {
        #region Variables

        private readonly ITestOutputHelper _output;

        #endregion Variables

        private HttpClient _client;

        public Tests(TestHelper<Startup> fixture
            , ITestOutputHelper output)
        {
            _client = fixture.Client;
            _output = output;
        }

        #region Positive Testing"

        [Fact]
        [Priority(1)]
        public async Task PostTestDeposit_Success()
        {
            var request = new
            {
                amount = 2000
            };
            var response = await _client.PostAsync("banking/deposit", ContentHelper.GetStringContent(request));
            _output.WriteLine("Result after deposit: {0}", response.Content.ReadAsStringAsync().Result);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        [Priority(2)]
        public async Task PostTestWithdraw_Success()
        {
            var request = new
            {
                amount = 1000
            };
            var response = await _client.PutAsync("banking/withdraw", ContentHelper.GetStringContent(request));
            string result = response.Content.ReadAsStringAsync().Result;
            _output.WriteLine("Result after withdrawn: {0}.", result);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        /// <summary>
        /// There is no record for id/npvId inputs
        /// </summary>
        /// <returns></returns>
        [Fact, Priority(6)]
        public async Task GetCurrentBalance_Success()
        {
            var response = await _client.GetAsync("banking");

            _output.WriteLine("This is output from {0}", response.StatusCode);
            _output.WriteLine("This is output from {0}", response.Content.ReadAsStringAsync().Result);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        #endregion Positive Testing

        #region Negative Testing

        [Theory, Priority(10)]
        [InlineData(200.20)]
        [InlineData(100000.40)]
        public async Task PostTestDeposit_Failed(double amountParam)
        {
            var request = new
            {
                amount = amountParam
            };
            var response = await _client.PostAsync("banking/deposit", ContentHelper.GetStringContent(request));
            _output.WriteLine("Error: {0}", response.Content.ReadAsStringAsync().Result);

            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Theory, Priority(11)]
        [InlineData(200.20)]
        [InlineData(20000)]
        public async Task PostTestWithdraw_Failed(double amountParam)
        {
            var request = new
            {
                amount = amountParam
            };
            var response = await _client.PutAsync("banking/withdraw", ContentHelper.GetStringContent(request));
            _output.WriteLine("Error: {0}", response.Content.ReadAsStringAsync().Result);

            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        #endregion Negative Testing
    }
}
