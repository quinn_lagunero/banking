﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Common.Cqrs
{
    /// <summary>
    /// Base class for common Query functionality
    /// </summary>
    public class BaseQuery
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is live.
        /// Used to determine whether client wants to force live data, rather than cache
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is live; otherwise, <c>false</c>.
        /// </value>
        public bool IsLive { get; set; }
        /// <summary>
        /// Gets or sets the maximum records.
        /// </summary>
        /// <value>
        /// The maximum records.
        /// </value>
        public int MaxRecords { get; set; }
        /// <summary>
        private string _appClientName;
        /// <summary>
        /// The name of the client/app connecting
        /// </summary>
        [JsonIgnore]
        public string AppClientName
        {
            get
            {
                if (string.IsNullOrEmpty(_appClientName))
                    _appClientName = "TEST_APP";

                return _appClientName;
            }
        }

        /// <summary>
        /// Adds the cache data.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data.</param>
        public virtual void AddCacheData<T>(T data)
        {
        }
    }

    /// <summary>
    /// Interface for all query input that execute Retrieve functions
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IQuery<TResult>
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is live.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is live; otherwise, <c>false</c>.
        /// </value>
        bool IsLive { get; set; }
    }

    /// <summary>
    /// Interface for all query execution of Retrieve functions.
    /// Parameter includes the IQuery input
    /// </summary>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        TResult Handle(TQuery query);
    }

    /// <summary>
    /// Interface for all query execution of Retrieve functions.
    /// This Query has no input params
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IQueryHandler<TResult>
    {
        /// <summary>
        /// Handles this instance.
        /// </summary>
        /// <returns></returns>
        TResult Handle();
    }

    // <summary>
    /// Mediator interface for queries
    /// </summary>
    public interface IQueryProcessor
    {
        /// <summary>
        /// Processes the specified query.
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        TResult Process<TResult>(IQuery<TResult> query);
        /// <summary>
        /// Processes query with no query input
        /// </summary>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns></returns>
        TResult Process<TResult>();
    }
}
