﻿using Bank.Common.Cqrs;
using Bank.Common.Cqrs.Validations;
using Bank.Domain.Model.Banking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Domain.Dal.Banking.Command.Deposit
{
    public class DepositCommandValidator : BaseCommandValidator, IValidate<DepositCommand>
    {
        public IReadOnlyCollection<IValidationResult> Validate(DepositCommand command)
        {
            if (command.Amount == 0)
                throw new ArgumentNullException("Deposit Amount", "Amount to be Deposit cannot be 0");

            if (command.Amount < TransactionRules.MinimumDeposit)
                throw new ArgumentNullException("Deposit Amount", string.Format("Amount to be Deposit:{0} cannot be less than {1}"
                    , command.Amount, TransactionRules.MinimumDeposit));

            if (command.Amount > TransactionRules.MaximumDeposit)
                throw new ArgumentNullException("Deposit Amount", string.Format("Amount to be Deposit: {0} exceeds maximum ammount of {1}" +
                    "", command.Amount, TransactionRules.MaximumDeposit));

            return new List<IValidationResult>();
        }
    }
}
