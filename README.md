# Mini-Banking App
 
#Projects and Solutions Description
1. Bank.Api - The API of the project.
2. Bank.Web - The User Interface of the project.
3. Bank.Common - Contains reusable codes for the project.
4. Bank.Domain - The Data Access Layer and objects model.
5. Bank.Test - The Test Automation of the API.

#Prerequisites 
1. MS Visual Studio 2017+ (Codes was written using VS 2019)
2. Please install .NET CORE SDK 2.2.204 or in later version.

#Cloning the project:
1. Kindly navigate to your local Drive Directory (Drive C: or D:)
2. Create a folder and right click inside (from the folder inside that you created) then choose Git Bash Here.
4. If you don't have Git Bash installed, kindly install it using this link: https://git-scm.com/downloads. Otherwise proceed to step 5.
4. Kindly paste: git clone https://quinn_lagunero@bitbucket.org/quinn_lagunero/banking.git
5. Wait until it finished.

#Running the applications
1. Change the ConnectionStrings.Banking_API from ..\Bank.Api\appsettings.json to the Bank.Api APP URL (can be found in Properties>Debug>App URL). Other option is you will see the localhost url once you run the Bank.Api.
2. Open and run the ..\Bank.Api and ..\Bank.Web solutions (.sln) respectively.
3. Ensure that the Api connections string are intact (otherwise go back to Step 1).
4. After you run the Bank.Api, you should be redirected to SWAGGER API Page (if not then kindly write it as http://[your localhost url and port]/swagger/ui/index.html).

#Running the Tests
1. Kindly stop the Bank.Api solution if its running (ideally it should be placed outside the Bank.Api).
2. Navigate to Test>>Windows>>Test Explorer from the Visual Studio menus.
3. You should be able to see Bank.Test(7) with 7 Tests
4. Click RUN ALL or Right click the Bank.Tests then choose "Run Selected Tests".