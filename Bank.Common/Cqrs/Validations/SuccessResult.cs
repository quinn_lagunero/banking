﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Bank.Common.Cqrs.Validations
{
    public class SuccessResult : IValidationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SuccessResult"/> class.
        /// </summary>
        public SuccessResult()
        {
        }

        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns></returns>
        bool IValidationResult.IsValid()
        {
            return true;
        }

        /// <summary>
        /// Gets the result message.
        /// </summary>
        /// <value>
        /// The result message.
        /// </value>
        public string ResultMessage
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the API status code.
        /// </summary>
        /// <remarks>
        /// If ever required then we can pass status code in constructor - if want differentiate OK, Created..etc
        /// </remarks>
        /// <value>
        /// The API status code.
        /// </value>
        public HttpStatusCode ApiStatusCode
        {
            get { return HttpStatusCode.OK; }
        }
    }
}
