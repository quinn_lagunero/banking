﻿using System;
using System.Collections.Generic;
using System.Text;
using Bank.Common.Cqrs.Validations;

namespace Bank.Common.Cqrs
{
    public class BaseCommand : ICommand
    {
        private IValidationResult _currentValidationResult = null;

        /// <summary>
        /// Gets or sets the current validation result.
        /// </summary>
        /// <value>
        /// The current validation result.
        /// </value>
        protected IValidationResult CurrentValidationResult
        {
            get
            {
                return this._currentValidationResult;
            }
            set
            {
                this._currentValidationResult = value;
            }
        }

        private bool _hasBeenValidated = false;

        /// <summary>
        /// Gets a value indicating whether this instance has been validated.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has been validated; otherwise, <c>false</c>.
        /// </value>
        //[JsonIgnore]
        public virtual bool HasBeenValidated
        {
            get
            {
                return this._hasBeenValidated;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        //[JsonIgnore]
        public bool IsValid
        {
            get
            {
                // todo: validation object
                return this._hasBeenValidated;
            }
        }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseCommand"/> class.
        /// </summary>
        public BaseCommand()
        {
        }

        #endregion Constructors

        #region Public

        /// <summary>
        /// Event fired prior to executing the command.
        /// </summary>
        /// <remarks>Should be implemented in each specific command</remarks>
        /// <returns></returns>
        public virtual IValidationResult BeforeExecute()
        {
            return new SuccessResult();
        }

        /// <summary>
        /// Event fired prior after executing the command
        /// </summary>
        /// <remarks>Should be implemented in each specific command</remarks>
        /// <returns></returns>
        public virtual IValidationResult AfterExecute()
        {
            return new SuccessResult();
        }

        #endregion Public
    }
}
